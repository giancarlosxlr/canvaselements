(function(){
    function calc(value, value2){
        var final;
        if(typeof value == 'string'){
            var percent = '%';
            var number = parseInt(value);
            if(value.indexOf(percent) != -1){
                var percent = number / 100;
                final = value2 * percent;

            }else{
                final = number;
            }
        }else{
            final = value;
        }

        return final;
    }

    function getRotate(x, y, cx, cy, angle){
        var radians = (Math.PI / 180) * angle,
            cos = Math.cos(radians),
            sin = Math.sin(radians),
            nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
            ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
        var novo = {x: nx, y:ny};
        return novo;
    }

    window.canvas = function(stance){
        var _this = this;
        _this.canvas = document.querySelector(stance);
        _this.width = 0;
        _this.height = 0;
        _this.ctx = _this.canvas.getContext('2d');
        var objects = [];
        var active;
        _this.mouse = {
            x: 0,
            y: 0
        };

        function loop(exe){
            for(var i=0;i<objects.length;i++){
                var obj = objects[i];
                exe(obj);
            }
        }

        _this.render = function(){
            _this.ctx.clearRect(0,0,_this.canvas.width,_this.canvas.height);
            loop(function(obj){
                obj.draw();
            });
        }

        function resize(){
            _this.render();
        }

        function getMouse(e){
            var rect = _this.canvas.getBoundingClientRect();
            _this.mouse.x = e.pageX - rect.left;
            _this.mouse.y = e.pageY - rect.top;
        }

        function isOver(obj,exe){
            var size = obj.getBoundingRect();
            var parents = obj.parents;
            var m = {x: _this.mouse.x, y: _this.mouse.y};
            for(var i=0;i<parents.length;i++){
                _pSize = parents[i].getBoundingRect();
                m = getRotate(m.x, m.y, _pSize.left + _pSize.origin.x, _pSize.top + _pSize.origin.y, _pSize.angle);
            }
            var m = getRotate(m.x, m.y, size.left + size.origin.x, size.top + size.origin.y, size.angle);
            if(m.x >= size.left && m.x <= size.left + size.width && m.y >= size.top && m.y <= size.top + size.height){
                exe();
            }
        }

        _this.createElement = function(obj){
            var novo = new container(_this);
            if(obj != undefined){
                novo.set(obj);
            }
            objects.push(novo);
            return novo;
        }

        function FalseReturn(){
            return false;
        }

        _this.canvas.addEventListener('click', function(e){
            (active.on.click || FalseReturn)();
        });

        _this.canvas.addEventListener('mousedown', function(e){
            (active.on.mousedown || FalseReturn)();
        });

        window.addEventListener('mouseup', function(e){
            (active.on.mouseup || FalseReturn)();
        });

        _this.canvas.addEventListener('mousemove', function(e){
            getMouse(e);
            var over = [];
            loop(function(obj){
                isOver(obj,function(){
                    over.push(obj);
                });
            });
            if(over.length > 0){
                active = over[over.length - 1];
                var cursor = active.cursor;
                _this.canvas.style.cursor = cursor;
                (active.on.mousemove || FalseReturn)();
            }else{
                _this.canvas.style.cursor = 'auto';
            }
        },false);
        window.addEventListener('resize', resize,false);
    }

    function container(stance){
        var _this = this;
        var ctx = stance.ctx;
        var img;
        _this.width = 0;
        _this.height = 0;
        _this.left = 0;
        _this.top = 0;
        _this.radius = 0;
        _this.opacity = 1;
        _this.rotate = 0;
        _this.parent = undefined;
        _this.parents = [];
        _this.globalCompositeOperation = 'source-over';
        _this.backgroundColor = 'rgba(0,0,0,0)';
        _this.borderColor = 'rgba(0,0,0,0)';
        _this.borderWidth = 1;
        _this.backgroundImage = undefined;
        _this.backgroundRepeat = 'repeat';
        _this.backgroundSize = 'auto auto';
        _this.originX = 50;
        _this.originY = 50;
        _this.scaleX = 1;
        _this.scaleY = 1;
        _this.cursor = 'auto';
        _this.drag = true;
        _this.offset = true;
        function setBg(){
            var bg = _this.backgroundImage;
            if(bg != undefined){
                var size = _this.get();
                var sizeBg = _this.backgroundSize.split(' ');
                var newWidth = sizeBg[0];
                var newHeight = sizeBg[1];
                var newImg = document.createElement('img');
                newImg.src = bg;
                var newBg = document.createElement('canvas');
                newImg.onload = function(){
                    if(newWidth != 'auto'){
                        if(newHeight == 'auto'){
                            var percentW = ((calc(newWidth, size.width)) / newImg.width) * newImg.height;
                            newImg.height = percentW;
                            newBg.height = percentW;
                        }
                        newImg.width = (calc(newWidth, size.width));
                        newBg.width = (calc(newWidth, size.width));
                    }else{
                        newBg.width = newImg.width;
                    }
                    if(newHeight != 'auto'){
                        if(newWidth == 'auto'){
                            var percentH = ((calc(newHeight, size.height)) / newImg.height) * newImg.width;
                            newImg.width = percentH;
                            newBg.width = percentH;
                        }
                        newImg.height = calc(newHeight, size.height);
                        newBg.height = calc(newHeight, size.height);
                    }else{
                        newBg.height = newImg.height;
                    }
                    var contextBg = newBg.getContext('2d');
                    contextBg.beginPath();
                    contextBg.drawImage(newImg,0,0,newImg.width,newImg.height);
                    img = newBg;
                    stance.render();
                }
            }
        }

        _this.set = function(obj){
            var keys = Object.keys(obj);
            for(i=0;i<keys.length;i++){
                var key = keys[i];
                _this[key] = obj[key];
            }
            setBg();
            setParent(_this);
        }

        function setParent(obj){
            var _parent = obj.parent;
            if(_parent != undefined){
                _this.parents.push(_parent);
                setParent(_parent);
            }else{
                _this.parents.reverse();
            }
        }

        _this.getBoundingRect = function(){
            var parent = _this.parents;
            var get = _this.get();
            var left = get.left;
            var top = get.top;
            for(var i=0;i<parent.length;i++){
                var _parent = parent[i];
                var calc = _parent.get();
                left += calc.left;
                top += calc.top;
            }
            var rect = {left: left, top: top, width: get.width, height: get.height, origin: get.origin, angle: _this.rotate};
            return rect;
        }

        _this.on = function(type, exe){
            _this.on[type] = function(){
                exe(_this);
            }
        }

        _this.get = function(){
            var width,height;
            var parent = _this.parent;
            if(parent){
                var size = parent.get();
                width = size.width;
                height = size.height;
            }else{
                width = ctx.canvas.width;
                height = ctx.canvas.height;
            }
            var w = calc(_this.width, width);
            var h = calc(_this.height, height);
            var x = calc(_this.left, width);
            var y = calc(_this.top, height);
            var origin = {x: calc(_this.originX,w), y: calc(_this.originY,h)};
            var get = {left: x, top: y, width: w, height: h, origin: origin};
            return get;
        }

        _this.draw = function(){
            var get = _this.get();
            var b = _this.borderWidth;
            var w = get.width;
            var h = get.height;
            var x = get.left;
            var y = get.top;
            var r = Math.min(Math.min(w, h) / 2, Math.max(0, _this.radius));
            var origin = get.origin;
            var parent = _this.parents;
            var len = parent.length;
            var op = 1;
            ctx.save();
            ctx.beginPath();
            ctx.strokeStyle = _this.borderColor;
            ctx.fillStyle = _this.backgroundColor;
            ctx.globalCompositeOperation = _this.globalCompositeOperation;
            ctx.lineWidth = b;
            if(len > 0){
                for(var i=0;i<len;i++){
                    var _parent = parent[i];
                    var _get = _parent.get();
                    var o = _get.origin;
                    ctx.translate(_get.left + o.x, _get.top + o.y);
                    ctx.scale(_parent.scaleX, _parent.scaleY);
                    ctx.rotate(_parent.rotate *Math.PI/180);
                    ctx.translate(-o.x,-o.y);
                    op = ((op * 100) * _parent.opacity) / 100;
                }
            }
            op = ((op * 100) * _this.opacity) / 100;
            ctx.translate(x + origin.x,y + origin.y);
            ctx.scale(_this.scaleX, _this.scaleY);
            ctx.globalAlpha = op;
            ctx.rotate(_this.rotate *Math.PI/180);
            ctx.translate(-origin.x,-origin.y);
            ctx.moveTo(r,0);
            ctx.lineTo(w-r,0);
            ctx.arcTo(w,0,w,h+r,r);
            ctx.lineTo(w,h-r);
            ctx.arcTo(w,h,w-r,h,r);
            ctx.lineTo(r,h);
            ctx.arcTo(0,h,0,h-r,r);
            ctx.lineTo(0,r);
            ctx.arcTo(0,0,r,0,r);
            ctx.closePath();
            ctx.stroke();
            ctx.fill();
            if(img != undefined){
                var pattern = ctx.createPattern(img, _this.backgroundRepeat);
                ctx.fillStyle = pattern;
                ctx.fill();
            }
            ctx.restore();
        }
    }
})();
